# Linux Networking Project

Author: [Ziyodulla Baykhanov](https://www.linkedin.com/in/ziyodulla-baykhanov/)

Date: 26th of May, 2024

Email: [ziyodulla_baykhanov@student.itpu.uz](mailto:ziyodulla_baykhanov@student.itpu.uz)

## Network Description

The purpose of this project is to create and configure simple networks using VirtualBox and Ubuntu.

The network design is provided in the image below.

![Network Design](screenshots/SE1101_Final_Project_02_hot_spots_3x.png)

**Host**: The host is the computer on which VirtualBox is running.

**Server**: A server is the virtual machine on which the Linux OS is deployed. Int1 of the Server is in the "Network Bridge" mode and is connected to the Net1 network. Address Int1 is in the address space of the home network. The IP address of Int1 is provided dynamically by the home Wi-Fi router using the DHCP service, for example, `192.168.1.100/24`. Interfaces Int2 and Int3 are connected in "Internal network" mode to networks Net2 and Net3, respectively.

**Client_1**: Client_1 is a Virtual machine with Linux OS. The interfaces Int1 and Int2 are connected in "Internal network" mode to Net2 and Net4 networks, respectively.

**Client_2**: Client_2 is a Virtual machine with Linux OS. The interfaces Int1 and Int2 are connected in "Internal network" mode to Net3 and Net4 networks, respectively.

To connect virtual machines in VirtualBox, I've chosen the address space for Net2, Net3, and Net4 networks according to the option number in the table below.

| Student Name | Student Surname | Net2 Address | Net3 Address  | Net4 Address   |
|:------------:|:---------------:|:------------:|:-------------:|:--------------:|
| First_Name   | Last_Name       | 10.A.B.0/24  | 172.20.C.0/24 | 192.168.D.0/24 |
| Ziyodulla    | Baykhanov       | 10.65.66.0/24| 172.20.67.0/24| 192.168.68.0/24|

So, the variables are set as follows:

- `First_Name`: Ziyodulla
- `Last_Name`: Baykhanov
- `A`: 65 (ASCII code for 'A')
- `B`: 66 (ASCII code for 'B')
- `C`: 67 (ASCII code for 'C')
- `D`: 68 (ASCII code for 'D')

## Tasks to be performed

### Creating and configuring a virtual machine

1. Create a new virtual machine with the name `Server`.

    **Solution:**

    Operating System on the Host Machine:

    ```powershell
    # On the Host. Display the operating system
    systeminfo | findstr /B /C:"OS Name" /C:"OS Version" /C:"OS Manufacturer" /C:"OS Configuration" /C:"OS Build Type"
    ```

    ![Host OS](screenshots/00_host_os.png)

    VirtualBox Version:

    ```powershell
    # On the Host. Display the VirtualBox version
    & "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" --version
    ```

    The following virtual machine was created in VirtualBox:

    ![VirtualBox Version](screenshots/01_host_vm_version.png)

    ![VirtualBox Version](screenshots/02_host_virtualbox_version.png)

2. Install OS Linux [Ubuntu distribution](https://ubuntu.com/download/desktop). Attention! During the installation process, set the username to your `First_Name` and the computer name to your `Last_Name`.

    **Result of this step**: Once the installation process is complete, open a terminal window and take a screenshot of it.

    **Solution:**

    The follpwing settings were used during the creation of the `Server` virtual machine:

    - Name: `Server`
    - Network Adapter 1: `Bridged Adapter` (Intel(R) Wi-Fi 6 AX200 160MHz)

    ![VirtualBox Server Settings](screenshots/03_host_virtualbox_server_settings.png)

    The following credentials were used during the installation process:

    - Username: `ziyodulla`
    - Computer name: `baykhanov`
    - Password: `123456`

    ![Ubuntu Installation](screenshots/04_host_ubuntu_installation.png)

    ```bash
    # On the Server. Check the release information
    lsb_release -a

    # On the Server. Display the username
    whoami

    # On the Server. Display the hostname
    hostname

    # On the Server. Display the computer name
    hostnamectl
    ```

    ![Server Terminal](screenshots/05_server_terminal.png)

    ```bash
    # On the Server. Update the package list
    sudo apt-get update
    ```

    ![Server Terminal](screenshots/06_server_terminal.png)

    ```bash
    # On the Server. Install traceroute utility
    sudo apt-get install traceroute

    # On the Server. Display the traceroute version
    traceroute --version
    ```

    ![Server Terminal](screenshots/07_server_terminal.png)

    ```bash
    # On the Server. Install the build-essential package
    sudo apt update
    sudo apt install -y build-essential linux-headers-$(uname -r)

    # On the Server. Install the VirtualBox Guest Additions
    cd /media/ziyodulla/VBox_GAs_7.0.18
    ./autorun.sh

    # On the Server. Reboot the virtual machine
    reboot
    ```

3. Make two clones of the virtual machine server with the names `Client-1` and `Client-2`.

    **Solution:**

    The following virtual machines were created by cloning the `Server` virtual machine:

    ![Client-1 Terminal](screenshots/08_host_virtualbox_client1.png)

    ![Client-2 Terminal](screenshots/09_host_virtualbox_client2.png)

4. Change the hostnames of `Client-1` and `Client-2` to `Client1-Last_Name` and `Client2-Last_Name` respectively.

    **Result of this step**: Open a terminal window and take a screenshot of it.

    **Solution:**

    ```bash
    # On Client-1. Setting the hostname to Client1-Baykhanov
    sudo hostnamectl set-hostname Client1-Baykhanov

    # On Client-1. Checking the hostname
    cat /etc/hostname
    ```

    ![Client-1 Hostname](screenshots/10_client1_terminal.png)

    ```bash
    # On Client-2. Setting the hostname to Client2-Baykhanov
    sudo hostnamectl set-hostname Client2-Baykhanov

    # On Client-2. Checking the hostname
    cat /etc/hostname
    ```

    ![Client-2 Hostname](screenshots/11_client2_terminal.png)

### Creating a network and configuring the IP address

5. Configure static addresses of the interfaces Int2 and Int3 on the server via Netplan. These addresses must be `10.A.B.1/24` and `172.20.C.1/24`, respectively.

    **Result of this step**: Take a screenshot of the configuration file (/etc/netplan/*.yaml in Ubuntu).

    **Solution:**

    Added additional network interfaces to the Server:
    - Adapter 2: `Internal Network` (Net2)
    - Adapter 3: `Internal Network` (Net3)

    ![VirtualBox Server Settings](screenshots/12_host_virtualbox_server_settings.png)
    ![VirtualBox Server Settings](screenshots/13_host_virtualbox_server_settings.png)

    ```bash
    # On the Server. Display the network interfaces
    ip addr
    ```

    ![Server IP Address](screenshots/14_server_ip.png)

    ```bash
    # On the Server. Edit the Netplan configuration file
    sudo nano /etc/netplan/01-network-manager-all.yaml
    ```

    Add the following configuration:

    ```yaml
    network:
      version: 2
      renderer: NetworkManager
      ethernets:
        enp0s3:
          dhcp4: true
        enp0s8:
          addresses:
            - 10.65.66.1/24 # Setting A = 65 and B = 66 for the 10.A.B.0/24 network
        enp0s9:
          addresses:
            - 172.20.67.1/24 # Setting C = 67 for the 172.20.C.0/24 network
    ```

    ```bash
    # On the Server. Show the Netplan configuration
    cat /etc/netplan/01-network-manager-all.yaml
    ```

    ![Netplan Configuration](screenshots/15_server_netplan.png)

    Apply the Netplan configuration:

    ```bash
    # On the Server. Apply the Netplan configuration
    sudo netplan apply
    ```

    ![Netplan Apply](screenshots/16_server_netplan_apply.png)

    ```bash
    # On the Server. Display the network interfaces
    ip addr
    ```

    ![Server IP Address](screenshots/17_server_ip.png)

6. Install the DHCP service on the Server.

    **Solution:**

    ```bash
    # On the Server. Install the DHCP service
    sudo apt-get install isc-dhcp-server
    ```

    ![DHCP Server Installation](screenshots/18_server_dhcp_installation.png)

7. Configure the DHCP service on the server:

    Create two subnets for Net2 and Net3:

    - Config the range for Net2 from `10.A.B.10` to `10.A.B.20` and option routers `10.A.B.1`;
    - Config the range for Net3 from `172.20.C.10` to `172.20.C.20` and option routers `172.20.C.1`.

    **Result of this step**: Take a screenshot of the configuration file (/etc/dhcp/dhcpd.conf).

    **Solution:**

    Edit the DHCP configuration file:

    ```bash
    # On the Server. Edit the DHCP configuration file
    sudo nano /etc/dhcp/dhcpd.conf
    ```

    Add the following configuration to dhcpd.conf:

    ```conf
    # Setting A = 65 and B = 66 for the 10.A.B.0/24 network for Net2
    subnet 10.65.66.0 netmask 255.255.255.0 {
      range 10.65.66.10 10.65.66.20;
      option routers 10.65.66.1;
    }

    # Setting C = 67 for the 172.20.C.0/24 network for Net3
    subnet 172.20.67.0 netmask 255.255.255.0 {
      range 172.20.67.10 172.20.67.20;
      option routers 172.20.67.1;
    }
    ```

    ![DHCP Configuration](screenshots/19_server_dhcpd_conf.png)

    Restart the DHCP server:

    ```bash
    # On the Server. Restart the DHCP server
    sudo systemctl restart isc-dhcp-server

    # On the Server. Check the status of the DHCP service
    sudo systemctl status isc-dhcp-server
    ```

    ![DHCP Server Status](screenshots/20_server_dhcp_status.png)

8. On Client-1 and Client-2, change the Adapter 1 settings in "attach to Internal network" and connect them to Net2 and Net3, respectively.

    **Solution:**

    On Client-1, the Adapter 1 settings were changed to "Internal network" and connected to Net2.
    ![Client-1 Network Adapter](screenshots/21_host_virtualbox_client1.png)

    On Client-2, the Adapter 1 settings were changed to "Internal network" and connected to Net3.
    ![Client-2 Network Adapter](screenshots/22_host_virtualbox_client2.png)

9. On Client-1 and Client-2, make changes in /etc/netplan/*.yaml to configure IP addresses dynamically.

    **Result of this step**: Take a screenshot of the configuration file (/etc/netplan/*.yaml).

    **Solution:**

    ```bash
    # On Client-1. Edit the Netplan configuration file
    sudo nano /etc/netplan/01-network-manager-all.yaml
    ```

    Add the following configuration:

    ```yaml
    network:
      version: 2
      renderer: NetworkManager
      ethernets:
        enp0s3:
          dhcp4: true
    ```

    ```bash
    # On Client-1. Show the Netplan configuration
    cat /etc/netplan/01-network-manager-all.yaml
    ```

    ![Client-1 Netplan Configuration](screenshots/23_client1_netplan.png)

    Apply the Netplan configuration:

    ```bash
    # On Client-1. Apply the Netplan configuration
    sudo netplan apply
    ```

    ![Client-1 Netplan Apply](screenshots/24_client1_netplan_apply.png)

    ```bash
    # On Client-2. Edit the Netplan configuration file
    sudo nano /etc/netplan/01-network-manager-all.yaml
    ```

    Add the following configuration:

    ```yaml
    network:
      version: 2
      renderer: NetworkManager
      ethernets:
        enp0s3:
          dhcp4: true
    ```

    ```bash
    # On Client-2. Show the Netplan configuration
    cat /etc/netplan/01-network-manager-all.yaml
    ```

    ![Client-2 Netplan Configuration](screenshots/25_client2_netplan.png)

    Apply the Netplan configuration:

    ```bash
    # On Client-2. Apply the Netplan configuration
    sudo netplan apply
    ```

    ![Client-2 Netplan Apply](screenshots/26_client2_netplan_apply.png)

### Network health check

10. On Client-1 and Client-2, check the IP addresses of interfaces (`ip addr` command) and make sure that the client machines have received the correct addresses.

    **Result of this step**: Take a screenshot of the output of the `ip addr` command.

    **Solution:**

    ```bash
    # On Client-1. Display the network interfaces
    ip addr
    ```

    ![Client-1 IP Address](screenshots/27_client1_ip.png)

    ```bash
    # On Client-2. Display the network interfaces
    ip addr
    ```

    ![Client-2 IP Address](screenshots/28_client2_ip.png)

11. From Client-1 and Client-2, check the connection with the server using the `ping` command.

    **Result of this step**: Take a screenshot of the `ping` output.

    **Solution:**

    ```bash
    # On the Server. Display the IP address of the network interfaces
    ip addr
    ```

    ![Server IP Address](screenshots/29_server_ip.png)

    ```bash
    # On Client-1. Ping the server
    ping -c 7 10.65.66.1
    ```

    ![Client-1 Ping Server](screenshots/30_client1_ping_server.png)

    ```bash
    # On Client-2. Ping the server
    ping -c 7 172.20.67.1
    ```

    ![Client-2 Ping Server](screenshots/31_client2_ping_server.png)

12. Switch to routing on the server by making the needed changes in the file /etc/sysctl.conf. After making changes to this file, you need to apply the changes for them to take effect.

    **Solution:**

    ```bash
    # On the Server. Edit the sysctl configuration file
    sudo nano /etc/sysctl.conf
    ```

    Uncomment the following line:

    ```conf
    net.ipv4.ip_forward=1
    ```

    ![IP Forwarding](screenshots/32_server_ip_forward.png)

    Apply the changes:

    ```bash
    sudo sysctl -p

    # On the Server. Check the IP forwarding status
    sysctl net.ipv4.ip_forward
    ```

    ![IP Forwarding](screenshots/33_server_ip_forward.png)

13. From Client-1, perform `traceroute` to the address of Int1 Client-2.

    **Result of this step**: Take a screenshot of the `traceroute` output.

    **Solution:**

    ```bash
    # On Client-2. Display the IP address of the enp0s3 interface
    ip addr show dev enp0s3
    ```

    ![Client-2 IP Address](screenshots/34_client2_ip.png)

    ```bash
    # On Client-1. Perform traceroute to the address of Int1 Client-2
    traceroute 172.20.67.10 # Since Client-2's Int1 received IP 172.20.67.10
    ```

    ![Client-1 Traceroute to Client-2](screenshots/35_client1_traceroute_client2.png)

    ```bash
    # On Client-1. The output of the traceroute command
    ziyodulla@Client1-Baykhanov:~$ traceroute 172.20.67.10
    traceroute to 172.20.67.10 (172.20.67.10), 30 hops max, 60 byte packets
    1  _gateway (10.65.66.1)  3.512 ms  4.570 ms  4.182 ms
    2  172.20.67.10 (172.20.67.10)  5.351 ms  5.230 ms  6.478 ms
    ```

### Additional network configuration (adding and configuring Net4)

14. In VirtualBox, add Int2 interfaces to Client-1 and Client-2 and connect them in "Internal network" mode to Net4.

    **Solution:**

    On Client-1, the Adapter 2 settings were changed to "Internal network" and connected to Net4.
    ![Client-1 Network Adapter Int2](screenshots/36_host_virtualbox_client1.png)

    On Client-2, the Adapter 2 settings were changed to "Internal network" and connected to Net4.
    ![Client-2 Network Adapter Int2](screenshots/37_host_virtualbox_client2.png)

15. In the Netplan configuration of Client-1 and Client-2, add static addresses for the following interfaces: Int2 of Client-1 - `192.168.D.1` and Int2 of Client-2 - `192.168.D.2`.

    **Solution:**

    ```bash
    # On Client-1. View the network interfaces before configuring the Netplan
    ip addr
    ```

    ![Client-1 IP Address Int2](screenshots/38_client1_ip_int2.png)

    ```bash
    # On Client-1. Edit the Netplan configuration file
    sudo nano /etc/netplan/01-network-manager-all.yaml
    ```

    Add the following configuration:

    ```yaml
    network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: true
        enp0s8:
          addresses:
            - 192.168.68.1/24 # Setting D = 68 in the 192.168.D.0/24 network for Net4
    ```

    ![Client-1 Netplan Configuration Int2](screenshots/39_client1_netplan_int2.png)

    Apply the Netplan configuration:

    ```bash
    # On Client-1. Apply the Netplan configuration
    sudo netplan apply
    ```

    ![Client-1 Netplan Apply Int2](screenshots/40_client1_netplan_apply_int2.png)

    ```bash
    # On Client-1. View the network interfaces after configuring the Netplan
    ip addr
    ```

    ![Client-1 IP Address Int2](screenshots/41_client1_ip_int2.png)

    ```bash
    # On Client-2. View the network interfaces before configuring the Netplan
    ip addr
    ```

    ![Client-2 IP Address Int2](screenshots/42_client2_ip_int2.png)

    ```bash
    # On Client-2. Edit the Netplan configuration file
    sudo nano /etc/netplan/01-network-manager-all.yaml
    ```

    Add the following configuration:

    ```yaml
    network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: true
        enp0s8:
          addresses:
            - 192.168.68.2/24 # Setting D = 68 in the 192.168.D.0/24 network for Net4
    ```

    ![Client-2 Netplan Configuration Int2](screenshots/43_client2_netplan_int2.png)

    Apply the Netplan configuration:

    ```bash
    # On Client-2. Apply the Netplan configuration
    sudo netplan apply
    ```

    ![Client-2 Netplan Apply Int2](screenshots/44_client2_netplan_apply_int2.png)

    ```bash
    # On Client-2. View the network interfaces after configuring the Netplan
    ip addr
    ```

    ![Client-2 IP Address Int2](screenshots/45_client2_ip_int2.png)

16. On Client-1 and Client-2, perform the `ip -br addr` command.

    **Result of this step**: Take a screenshot of the output.

    **Solution:**

    ```bash
    # On Client-1. Display the network interfaces
    ip -br addr
    ```

    ![Client-1 IP -br addr](screenshots/46_client1_ip_br_addr.png)

    ```bash
    # On Client-2. Display the network interfaces
    ip -br addr
    ```

    ![Client-2 IP -br addr](screenshots/47_client2_ip_br_addr.png)

17. From Client-1, `ping 192.168.D.2`.

    **Result of this step**: Take a screenshot of the output.

    **Solution:**

    ```bash
    # On Client-1. Ping 192.168.C.2
    ping -c 7 192.168.68.2 # Setting D = 68 in the 192.168.D.0/24 network for Net4
    ```

    ![Client-1 Ping Client-2](screenshots/48_client1_ping_client2.png)

### Configuring and testing IP routing

18. On Client-1 and Client-2, perform the `ip route` command.

    **Result of this step**: Take a screenshot of the output.

    **Solution:**

    ```bash
    # On Client-1. Display the IP route
    ip route
    ```

    ![Client-1 IP Route](screenshots/49_client1_ip_route.png)

    ```bash
    # On Client-2. Display the IP route
    ip route
    ```

    ![Client-2 IP Route](screenshots/50_client2_ip_route.png)

19. On Client-1, using the `ip route` command, configure the route to Net3 via Client-2. On Client-2, using the same command, configure the route to Net2 via Client-1.

    **Result of this step**: Take TWO screenshots of the output.

    **Solution:**

    ```bash
    # On Client-1. Configure the route to Net3 via Client-2
    sudo ip route add 172.20.67.0/24 via 192.168.68.2

    # On Client-1. Check the IP route
    ip route
    ```

    ![Client-1 IP Route](screenshots/51_client1_ip_route.png)

    ```bash
    # On Client-2. Configure the route to Net2 via Client-1
    sudo ip route add 10.65.66.0/24 via 192.168.68.1

    # On Client-2. Check the IP route
    ip route
    ```

    ![Client-2 IP Route](screenshots/52_client2_ip_route.png)

20. From Client-1, perform `traceroute` to the address of Int1 Client-2.

    **Result of this step**: Take a screenshot of the output. Compare it with the output in step 13 and explain the difference.

    **Solution:**

    ```bash
    # On Client-2. Display the IP address of the enp0s3 interface
    ip addr show dev enp0s3
    ```

    ![Client-2 IP Address](screenshots/53_client2_ip.png)

    ```bash
    # On Client-1. Perform traceroute to the address of Int1 Client-2
    traceroute 172.20.67.10
    ```

    ![Client-1 Traceroute to Client-2](screenshots/54_client1_traceroute_client2.png)

    ```bash
    # On Client-1. The output of the traceroute command
    ziyodulla@Client1-Baykhanov:~$ traceroute 172.20.67.10
    traceroute to 172.20.67.10 (172.20.67.10), 30 hops max, 60 byte packets
    1  172.20.67.10 (172.20.67.10)  2.374 ms  2.852 ms  2.505 ms
    ```

    ```bash
    # Compare with the output in step 13:
    ziyodulla@Client1-Baykhanov:~$ traceroute 172.20.67.10
    traceroute to 172.20.67.10 (172.20.67.10), 30 hops max, 60 byte packets
    1  _gateway (10.65.66.1)  3.512 ms  4.570 ms  4.182 ms
    2  172.20.67.10 (172.20.67.10)  5.351 ms  5.230 ms  6.478 ms
    ```

    The difference is that in step 13, the `traceroute` command from Client-1 to Client-2's goes through the default gateway, while in step 20, the `traceroute` command from Client-1 to Client-2's goes directly through the route configured in step 19.

### Configuring and testing SSH

21. On the server, install the SSH server.

    **Solution:**

    ```bash
    # On the Server. Update the package list
    sudo apt update

    # On the Server. Install the SSH server
    sudo apt install openssh-server
    ```

    ![SSH Server Installation](screenshots/55_server_ssh_installation.png)
    ![SSH Server Installation](screenshots/56_server_ssh_installation.png)

    ```bash
    # On the Server. Start the SSH server
    sudo systemctl start ssh

    # On the Server. Enable the SSH server
    sudo systemctl enable ssh

    # On the Server. Check the status of the SSH server
    sudo systemctl status ssh
    ```

    ![SSH Server Status](screenshots/57_server_ssh_status.png)

22. From Client-1, connect to the server via SSH. After connecting successfully, disconnect using the `exit` command.

    **Result of this step**: Take a screenshot of the output of the `exit` command, capturing the command prompt that appears.

    **Solution:**

    ```bash
    # On the Server. Display the IP address of the network interfaces
    ip -br addr
    ```

    ![Server IP Address](screenshots/58_server_ip_br_addr.png)

    ```bash
    # On Client-1. Display the hostname
    hostname

    # On Client-1. Connect to the Server via SSH
    ssh ziyodulla@192.168.0.22

    # On the Server. Display the hostname
    hostname

    # On the Server. Exit from the SSH session
    exit
    ```

    ![Client-1 SSH to Server](screenshots/59_client1_ssh_exit.png)

23. From Client-2, connect to the server via SSH. After connecting successfully, disconnect using the `exit` command.

    **Result of this step**: Take a screenshot of the output of the `exit` command, capturing the command prompt that appears.

    **Solution:**

    ```bash
    # On the Server. Display the IP address of the network interfaces
    ip -br addr
    ```

    ![Server IP Address](screenshots/58_server_ip_br_addr.png)

    ```bash
    # On Client-2. Display the hostname
    hostname

    # On Client-2. Connect to the Server via SSH
    ssh ziyodulla@192.168.0.22

    # On the Server. Display the hostname
    hostname

    # On the Server. Exit from the SSH session
    exit
    ```

    ![Client-2 SSH to Server](screenshots/60_client2_ssh_exit.png)

### Configuring and testing iptables

24. Remove the routes created in Step 19 from the routing tables of Client-1 and Client-2. Check the result using the `ip route` command. The output must be the same as in step 18.

    **Solution:**

    ```bash
    # On Client-1. Display the IP route
    ip route

    # On Client-1. Remove the route to Net3 via Client-2
    sudo ip route del 172.20.67.0/24 via 192.168.68.2

    # On Client-1. Check the IP route after removing the route to Net3 via Client-2
    ip route
    ```

    ![Client-1 IP Route](screenshots/61_client1_ip_route.png)

    Compare with the output in step 18:

    ![Client-1 IP Route](screenshots/49_client1_ip_route.png)

    The output on the Client-1 is the same as in step 18.

    ```bash
    # On Client-2. Display the IP route
    ip route

    # On Client-2. Remove the route to Net2 via Client-1
    sudo ip route del 10.65.66.0/24 via 192.168.68.1
    
    # On Client-2. Check the IP route after removing the route to Net2 via Client-1
    ip route
    ```

    ![Client-2 IP Route](screenshots/62_client2_ip_route.png)

    Compare with the output in step 18:

    ![Client-2 IP Route](screenshots/50_client2_ip_route.png)

    The output on the Client-2 is the same as in step 18.

25. On Client-2, using the `ip addr` command, assign Int1 the extra IP-address `172.20.C.50/24`. Using the `ip addr` command displays all the addresses of Int1.

    **Result of this step**: Take a screenshot of the output.

    **Solution:**

    ```bash
    # On Client-2. Assign the extra IP-address 172.20.C.50/24 to Int1
    sudo ip addr add 172.20.67.50/24 dev enp0s3 # Setting C = 67 for the 172.20.C.0/24 network
    ip addr show dev enp0s3
    ```

    ![Client-2 IP Addr](screenshots/63_client2_ip_addr.png)

26. Configure the firewall on Server as follows:

    - Deny connection to Server via SSH from Client-2.
    - Deny pings from Client-1 to Server.
    - Deny pings from Client-1 to `172.20.C.50`.

    Check the result of trying to connect to Server_1 via SSH from Client_2, a ping from Client_1 to Server_1, and a ping from Client_1 to `172.20.C.50`.

    **Result of this step**: Take a screenshot of the output of the `iptables -L` command.

    **Solution:**

    ```bash
    # On the Server. Display the IP address of the network interfaces
    ip -br addr
    ```

    ![Server IP Address](screenshots/64_server_ip_br_addr.png)

    ```bash
    # On Client-1. Display the IP address of the network interfaces
    ip -br addr
    ```

    ![Client-1 IP Address](screenshots/65_client1_ip_br_addr.png)

    ```bash
    # On Client-2. Display the IP address of the network interfaces
    ip -br addr
    ```

    ![Client-2 IP Address](screenshots/66_client2_ip_br_addr.png)

    ```bash
    # On the Server. Configure the firewall
    
    # Deny connection to Server via SSH from Client-2
    sudo iptables -A INPUT -p tcp --dport 22 -s 172.20.67.10 -j REJECT # Here 172.20.67.10 is the IP address of Client-2's Int1

    # Deny pings from Client-1 to Server
    sudo iptables -A INPUT -p icmp --icmp-type echo-request -s 10.65.66.10 -j REJECT # Here 10.65.66.10 is the IP address of Client-1's Int1

    # Deny pings from Client-1 to 172.20.C.50
    sudo iptables -A FORWARD -s 10.65.66.10 -d 172.20.67.50 -p icmp --icmp-type echo-request -j REJECT # Here 172.20.67.50 is the IP address of Client-2's Int1

    # On the Server. Save the firewall configuration
    sudo iptables-save
    ```

    ![IPTables Configuration](screenshots/67_server_iptables.png)

    ```bash
    # On the Server. Check the status of the firewall
    sudo iptables -L
    ```

    ![IPTables Configuration](screenshots/68_server_iptables.png)

    ```bash
    # On Client-2. Try to connect to Server via SSH
    ssh ziyodulla@192.168.0.22
    ```

    ![Client-2 SSH Blocked](screenshots/69_client2_ssh_blocked.png)

    ```bash
    # On Client-1. Try to ping Server
    ping -c 7 10.65.66.1
    ```

    ![Client-1 Ping Server Blocked](screenshots/70_client1_ping_server_blocked.png)

    ```bash
    # On Client-1. Try to ping 172.20.C.50
    ping -c 7 172.20.67.50 # Setting C = 67 for the 172.20.C.0/24 network
    ```

    ![Client-1 Ping 172.20.67.50 Blocked](screenshots/71_client1_ping_172_20_67_50_blocked.png)

27. On Server, configure the NAT service so that Client-1 and Client-2 may ping internet resources, for example, `8.8.8.8`. Check the result of trying to `ping 8.8.8.8` from Client-1 and Client-2.

    **Result of this step**: Take two screenshots:

    1) The output of pinging the `8.8.8.8` command on Client_1 and Client_2.
    2) The output of the `iptables` command, showing the results of the NAT configuration.

    **Solution:**

    ```bash
    # On the Server. Configure the NAT service

    # Enable NAT
    sudo iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE

    # Allow forwarding
    sudo iptables -A FORWARD -i enp0s8 -o enp0s3 -j ACCEPT
    sudo iptables -A FORWARD -i enp0s3 -o enp0s8 -m state --state RELATED,ESTABLISHED -j ACCEPT

    sudo iptables -A FORWARD -i enp0s9 -o enp0s3 -j ACCEPT
    sudo iptables -A FORWARD -i enp0s3 -o enp0s9 -m state --state RELATED,ESTABLISHED -j ACCEPT
    ```

    ![NAT Configuration](screenshots/72_server_nat.png)

    ```bash
    # Save iptables rules
    sudo iptables-save
    ```

    ![NAT Configuration](screenshots/73_server_nat.png)

    ```bash
    # Enable IP forwarding
    sudo sysctl -w net.ipv4.ip_forward=1

    # Check the status of IP forwarding
    cat /proc/sys/net/ipv4/ip_forward

    # Apply the changes
    sudo sysctl -p
    ```

    ![NAT Configuration](screenshots/74_server_nat.png)

    ```bash
    # On Client-1. Ping the internet resource
    ping -c 7 8.8.8.8
    ```

    ![Client-1 Ping](screenshots/75_client1_ping.png)

    ```bash
    # On Client-2. Ping the internet resource
    ping -c 7 8.8.8.8
    ```

    ![Client-2 Ping](screenshots/76_client2_ping.png)

    ```bash
    # On the Server. Check the status of the NAT service
    sudo iptables -L -v -n
    sudo iptables -t nat -L -v -n
    ```

    ![NAT Configuration](screenshots/77_server_nat.png)
